const {google} = require('googleapis');
const express = require("express")
const fs = require('fs');
const reports = require("./reportingservice")
const app = express()
const SERVER_PORT = 4850
const clientid = "1074142234859-jigiimhcvrcfgui16invh29cfv92ffau.apps.googleusercontent.com"
const clientsecret = "BA0re0DtUZYaYAOpn1AzVQFU"
const redirecturi = "https://www.insuredmine.info/ga/api/garedirect"
const dbconfig = require("./src/config/dbconfig")
const mongoose = require("mongoose")
const cors = require("cors")
//let agendaConfig = require("./src/config/agenda.config")
var bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(cors())

//agendaConfig(app)
const customroute = express.Router();

let oAuth2Client = new google.auth.OAuth2(
    clientid,
    clientsecret,
    redirecturi
);

/*

mongoose.connect(dbconfig.mongo.uri, dbconfig.mongo.options);

mongoose.connection.on('connected', function(obj) {
  console.error(`mongodb connected ${obj}`);
});

mongoose.connection.on('error', function(err) {
  console.error(`mongodb connection error: ${err}`);
  process.exit(-1); // eslint-disable-line no-process-exit
});
*/
mongoose.connect(dbconfig.mongo.uri, dbconfig.mongo.options).then(
    (dbobj) => { 
        console.log("mongodb connection success")
    },
    err => { 
        console.error(`mongodb connection error: ${err}`);
    }
);

global.access_token = "";
let analyticsreporting = null

const authenticate = (scopes) => {
    console.log("in authenticate()")
    let authorizeUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: scopes.join(' '),
    });
    console.log("authorizeUrl ",authorizeUrl)
    return authorizeUrl
}
customroute.get("/test",(req,res)=>{
    console.log("in /ga/test")
    res.send({status : "from /ga/test"})
})
customroute.get("/usega",(req,res)=>{
    console.log("in /ga/usega")
    const scopes = ['https://www.googleapis.com/auth/analytics'];
    
    let authorizeUrl = authenticate(scopes)
    res.redirect(authorizeUrl)

    console.log("in end of /ga/usega")
})

customroute.get("/garedirect",async (req,res)=>{
    console.log("in /ga/api/garedirect")
    const reqquery = req.query
    console.log("reqquery ",reqquery)
    console.log("reqquery.code ",reqquery.code)
    const {tokens} = await oAuth2Client.getToken(reqquery.code)
    console.log("tokens ",tokens)
    oAuth2Client.credentials =tokens
    global.access_token = tokens.access_token
    console.log("global.access_token ",global.access_token)
    analyticsreporting = google.analyticsreporting({
        version: 'v4',
        auth: oAuth2Client
    });
    console.log("tokens are set in oAuth2Client.credentials")
    console.log(oAuth2Client)
    res.send({"status" : "from /ga/api/garedirect"})
})

customroute.get("/sample",async (req,res)=>{
    console.log("in /sample")
    let result = await reports.sample(analyticsreporting)
    res.send({"status" : "/sample"})
})

customroute.get("/hostname",async (req,res)=>{
    console.log("in /hostname")
    let result = await reports.hostname(analyticsreporting)
    res.send({"status" : "/hostname",result})
})
customroute.get("/userid",async (req,res)=>{
    console.log("in /userid")
    let result = await reports.userid(analyticsreporting)
    res.send({"status" : "/userid",result})
})


app.use("/ga/api",customroute);
//app.use("/ga/api/zoo",require("./src/routes").zooRoutes);
app.use("/ga/api/pull",require("./src/routes").gaRoutes);
//app.use("/ga/api/agenda",require("./src/routes").agendaRoutes);
//app.use("/ga/api/bull",require("./src/routes").bullRoutes);

app.listen(SERVER_PORT,()=>{
    console.log("server is listening on port ",SERVER_PORT)
})