const axios = require("axios")
const sample = async(analyticsreporting)=> {
    const res = await analyticsreporting.reports.batchGet({
      requestBody: {
        reportRequests: [
          {
            viewId: '209714694',
            dateRanges: [
              {
                startDate: '2020-01-24',
                endDate: '2020-01-27',
              }
            ],
            metrics: [
              {
                expression: 'ga:users',
              },
            ],
          },
        ],
      },
    });
    console.log(res.data);
    let {columnHeader,data} = res.data.reports[0]
    console.log("columnHeader ",columnHeader)
    console.log("data ",data)

    console.log("stringify-columnHeader ",JSON.stringify(columnHeader))
    console.log("stringify-data ",JSON.stringify(data))
    return res.data;
}
const hostname = async(analyticsreporting)=> {
    const res = await analyticsreporting.reports.batchGet({
      requestBody: {
        reportRequests: [
          {
            viewId: '209714694',
            dimensions :[
            {
                "name" : "ga:pagePath"
            },
            {
                "name": "ga:hostname"
            },
            {
                "name" : "ga:exitPagePath"
            },
            {
              "name"  : "ga:city"
            },
            {
              "name" : "ga:cityId"
            }
          ],
            metrics:[
                {
                    "expression" : "ga:pageviews"
                },
                {
                    "expression" : "ga:uniquePageviews"
                },
                {
                    "expression" : "ga:bounceRate"
                },
                {
                    "expression" : "ga:avgTimeOnPage"
                },
                {
                  "expression" : "ga:timeOnScreen"
                },
                {
                  "expression" :  "ga:timeOnPage"
                }
            ],
            dateRanges: [
              {
                startDate: '2020-02-03',
                endDate: '2020-02-03',
              }
            ]
          },
        ],
      },
    });
    console.log(res.data);
    let {columnHeader,data} = res.data.reports[0]
    console.log("stringify-columnHeader ",JSON.stringify(columnHeader))
    console.log("stringify-data ",JSON.stringify(data))

    return res.data;
}
const useridBackup = async(analyticsreporting)=>{
    const res = await analyticsreporting.userActivity.search({
        requestBody: {
            reportRequests: [
                {
                    "dateRange": 
                      {
                    "startDate": "2020-01-24",
                    "endDate": "2020-01-25"
                      }
                      ,
                    "viewId": "209714694",
                    "user": {
                      "type" : "USER_ID",
                      "userId" : "demo@insuredmine.com"
                    },
                    "pageSize" : 10
                  }
            ]
        }
    })
    console.log(res.data);
    let {columnHeader,data} = res.data.reports[0]
    
    console.log("stringify-columnHeader ",JSON.stringify(columnHeader))
    console.log("stringify-data ",JSON.stringify(data))
    return res.data;
}
const userid = async(analyticsreporting)=>{
  let url = "https://analyticsreporting.googleapis.com/v4/userActivity:search"
  let body = {
    "dateRange": 
      {
    "startDate": "2020-02-01",
    "endDate": "2020-02-01"
      }
      ,
    "viewId": "209714694",
    "user": {
      "type" : "USER_ID",
      "userId" : "nick@breweriigroup.com"
    },
    "pageSize" : 100
  };
  console.log("reportingservice.js - global.access_token ",global.access_token)

  let authToken = global.access_token
  let headers = {
    headers: {
      Authorization: 'Bearer ' + authToken
    }
  }
  let userResponse = null
  try{
  userResponse = await axios.post(url,body,headers)
  console.log("userResponse.data " ,userResponse.data)
  }
  catch(e){
    console.log("userid catchblock ",e)
  }
  return userResponse.data;
}
module.exports={
    sample,hostname,userid
}