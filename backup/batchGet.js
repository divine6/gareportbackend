
'use strict';

const {google} = require('googleapis');
const sampleClient = require('./sampleclient');

const analyticsreporting = google.analyticsreporting({
  version: 'v4',
  auth: sampleClient.oAuth2Client,
});

async function runSample() {
  const res = await analyticsreporting.reports.batchGet({
    requestBody: {
      reportRequests: [
        {
          viewId: '209714694',
          dateRanges: [
            {
              startDate: '2020-01-24',
              endDate: '2020-01-27',
            }
          ],
          metrics: [
            {
              expression: 'ga:users',
            },
          ],
        },
      ],
    },
  });
  console.log(res.data);
  return res.data;
}

// if invoked directly (not tests), authenticate and run the samples
if (module === require.main) {
  const scopes = ['https://www.googleapis.com/auth/analytics'];
  sampleClient
    .authenticate(scopes)
    .then(runSample)
    .catch(console.error);
}

// export functions for testing purposes
module.exports = {
  runSample,
  client: sampleClient.oAuth2Client,
};