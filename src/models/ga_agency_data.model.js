const mongoose = require("mongoose")
const Schema = mongoose.Schema

const ga_agency_data_schema = new Schema({
    city : String,
    latitude : String,
    longitude : String,
    hostname: String,
    pagePath: String,
    cityId: String,
    countryIsoCode: String,
    noOfusers:String,
    pageviews: String,
    timeOnPage: String,
    reportDate: {
        type:String
    },
    createdAt: {
        type:Date,
        default: Date.now
    }
})

module.exports = mongoose.model("ga_agency_data",ga_agency_data_schema) 