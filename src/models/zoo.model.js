const mongoose = require("mongoose")
const Schema = mongoose.Schema

const zoo_schema = new Schema({
    name : {type : String},
    location : {type:String}
})

module.exports = mongoose.model("zoo",zoo_schema) 