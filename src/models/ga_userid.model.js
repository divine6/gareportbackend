const mongoose = require("mongoose")
const Schema = mongoose.Schema

const ga_userid_schema = new Schema({
    userid : {
        type : String    
    },
    sessions : {type:String},
    averageSessionDuration : {type:String},
    reportdate:String,
    isactive : {
        type : Boolean,
        default:true
    },
    updatedAt:{
        type:Date,
        default:Date.now
    },
    hostname : String,
    activity:[{
        sessionId : String,
        activityTime : String,
        activityType : String,
        landingPagePath: String,
        pagePath: String,
        pageTitle: String,
        reportdate:String
    }],
})
ga_userid_schema.index({ userid: 1, reportdate: 1 }, { unique: true })

module.exports = mongoose.model("ga_userid",ga_userid_schema) 