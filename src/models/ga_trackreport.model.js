const mongoose = require("mongoose")
const Schema = mongoose.Schema



const ga_track_report_schema= new Schema({
    reportdate : {
        type : String,
        unique:true
    },
    agencyreport:{
        type : String,
        enum:["notstarted","completed"]
    },
    csvreport:{
        type : String,
        enum:["notstarted","completed"]
    },
    updatedAt : {
        type:Date,
        default: Date.now
    }
})
module.exports=mongoose.model("ga_track_report",ga_track_report_schema)