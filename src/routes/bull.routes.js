const express = require("express")
const emailQueue = require("./../config/bull.config")
const { UI } = require('bull-board')
const router = express.Router()

router.get("/addjob",(req,res)=>{
    console.log("/addjob")
    let data = req.query.data
    emailQueue.add("lionjob",{data: data});
    res.send({status : "/addjob"})
})

router.get("/pausejob",(req,res)=>{
    console.log("/pausejob")
    //emailQueue.add({data: "pausing the job"});
    emailQueue.pause().then(function(){
        // queue is paused now
        console.log("queue is paused now")
    });
    res.send({status : "/pausejob"})
})
router.get("/resumejob",(req,res)=>{
    console.log("/resumejob")
    //emailQueue.add({data: "pausing the job"});
    emailQueue.resume().then(function(){
        // queue is paused now
        console.log("queue is resume now")
    });
    res.send({status : "/resumejob"})
})
router.use("/ui",UI)

module.exports=router