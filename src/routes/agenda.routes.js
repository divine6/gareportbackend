const express = require("express")
const router = express.Router()
const agendaController = require("./../controller/agenda.controller")
const agendaDash =require("./../config/agendadash.config")

//agenda
console.log("agenda routes")
router.get("/purge",agendaController.purge)
router.get("/immediate",agendaController.immediate)
router.get("/scheduleafter",agendaController.scheduleafter)

router.get("/scheduleafterheavy",agendaController.scheduleafterheavy)
router.get("/stopagenda",agendaController.stopagenda)
router.get("/startagenda",agendaController.startagenda)


router.use("/dash",agendaDash)

module.exports=router