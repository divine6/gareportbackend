const express = require("express")
const gacontroller = require("./../controller/ga.controller")
const router = express.Router()

//check if reportdate property is present in queryParam
const reportDateCheck=(req,res,next)=>{
    console.log("in reportDateCheck ",req.query)
    let reqquery = req.query
    if(reqquery.hasOwnProperty("reportdate")){
        console.log("reportdate present in queryparam")
        next()
    }
    else{
        res.send({"status" : "please send reportdate parameter via queryparam"})
    }
}

const reportDateUserIdCheck=(req,res,next)=>{
    console.log("in reportDateCheck ",req.query)
    let reqquery = req.query
    if(reqquery.hasOwnProperty("reportdate") && reqquery.hasOwnProperty("userid")){
        console.log("reportdate,userid present in queryparam")
        next()
    }
    else{
        res.send({"status" : "please send reportdate,userid parameter via queryparam"})
    }
}
const reportFromToDateCheck = (req,res,next)=>{
    console.log("in reportDateCheck ",req.query)
    let reqquery=req.query
    if(reqquery.hasOwnProperty("fromdate") && reqquery.hasOwnProperty("todate")){
        console.log("fromdate,todate present in queryparam")
        next()
    }
    else{
        res.send({"status" : "please send fromdate,todate parameter via queryparam"})
    }
}
router.get("/loadcsvdata",reportDateCheck,gacontroller.loadcsvData)
router.get("/loadagencydata",reportDateCheck,gacontroller.loadAgencyData)

router.get("/fetchagencydata",reportFromToDateCheck,gacontroller.fetchagencydata)
router.get("/fetchagentdata",reportFromToDateCheck,gacontroller.fetchagentdata)

router.delete("/csvreport",reportDateCheck,gacontroller.deletecsvreport)
router.delete("/agencyreport",reportDateCheck,gacontroller.deleteagencyreport)



router.get("/downloadagencydata",gacontroller.downloadagencydata)
router.get("/downloadagentdata",gacontroller.downloadagentdata)
router.get("/downloadagentdatawithactivity",gacontroller.downloadagentdatawithactivity)


router.get("/pulldatabasedonuserid",reportDateUserIdCheck,reportDateCheck,gacontroller.pullDataBasedOnUserId)
router.post("/blacklist",gacontroller.postBlacklist)

module.exports=router