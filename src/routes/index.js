const gaRoutes = require("./ga.routes")
const agendaRoutes = require("./agenda.routes")
const bullRoutes = require("./bull.routes")
const zooRoutes = require("./zoo.route")
module.exports={
    gaRoutes,agendaRoutes,bullRoutes,zooRoutes
}