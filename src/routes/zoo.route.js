const express = require("express")
const controller = require("./../controller/zoo.controller")
const router = express.Router()

router.get("/data",controller.dataget)
router.post("/data",controller.datapost)
router.put("/data",controller.dataupdate)


module.exports=router