const gaService = require("./../service/ga.service")
//const json2csv = require('json2csv').parse;
const { parse } = require('json2csv');

//loadreportcsv containing userid for a specific date
const loadcsvData= async (req,res)=>{
    console.log("loadcsvData req.query ",req.query)
    let reportdate=req.query.reportdate
    let finddata={
        reportdate : reportdate    
    }
    let reportStatus = await gaService.isReportDateExist(finddata)
    console.log("reportStatus ",reportStatus)
    if(reportStatus && reportStatus["csvreport"] === "completed"){
        res.send({status : "csvdata already loaded for date "+reportdate})
    }
    else {
        try{
            await gaService.loadCsv(reportdate)
            let inputdata={
                ...finddata,
                csvreport : "completed"
            }
            await gaService.insertReportMetadata(finddata,inputdata)
            res.send({status : "csvdata loaded successfully-reportdate "+reportdate})
        }
        catch(e){
            res.send({status : "csvdata load failed-reportdate "+reportdate})
        }
    }
}
const loadcsvDataWireFloodData=async ()=>{
    let bulkinputItems = [
        { reportdate:"2020-02-16" , filename :'./seed/useriddump/2020-02-16-dump.csv'}
    ]
    for(let i=0;i<bulkinputItems.length;i++){
        await loadcsvDataWire(bulkinputItems[i].reportdate,bulkinputItems[i].filename)
    }
}
const loadcsvDataWire= async (reportdate,filename)=>{
    console.log("in loadcsvDataWire reportdate ",reportdate, " filename ",filename)

    let finddata={
        reportdate : reportdate    
    }
    let reportStatus = await gaService.isReportDateExist(finddata)
    console.log("reportStatus ",reportStatus)
    if(reportStatus && reportStatus["csvreport"] === "completed"){
        console.log({status : "csvdata already loaded for date "+reportdate})
    }
    else {
        try{
            await gaService.loadCsv(reportdate,filename)
            let inputdata={
                ...finddata,
                csvreport : "completed"
            }
            await gaService.insertReportMetadata(finddata,inputdata)
            console.log({status : "csvdata loaded successfully-reportdate "+reportdate})
        }
        catch(e){
            console.log({status : "csvdata load failed-reportdate "+reportdate})
        }
    }
}

/*
setTimeout(async ()=>{
    console.log("85 start")
    await loadcsvDataWireFloodData()
    console.log("87 end")
},3000)
*/
const loadAgencyData = async (req,res)=>{
    console.log("loadAgencyData req.query ",req.query)
    let reportdate=req.query.reportdate

    let finddata={
        reportdate : reportdate    
    }
    let reportStatus = await gaService.isReportDateExist(finddata)
    console.log("reportStatus ",reportStatus)
    if(reportStatus && reportStatus["agencyreport"] === "completed"){
        res.send({status : "agencydetails already loaded for date "+reportdate})
    }
    else{
        console.log("before fetchagency")
        try{
            await gaService.etlAgenciesFromExternalSource(reportdate)
            console.log("after fetchagency")
            let inputdata={
                ...finddata,
                agencyreport : "completed"
            }
            await gaService.insertReportMetadata(finddata,inputdata)
            res.send({status : "agencydetails loaded successfully-reportdate "+reportdate})
        }
        catch(e){
            console.log("agencyload failed ",e.message)
            res.send({status : "agencydetails load failed reportdate "+reportdate+" message "+e.message})
        }        
    }
}

//store blacklist details into database
const postBlacklist= async(req,res)=>{
    console.log("blacklist req.body ",req.body)
    let reqbody = req.body
    //string or comma separated string "data.in,data.com"
    
    let response=[];

    if(reqbody.hasOwnProperty("hostname")){
        let hostname = reqbody.hostname
        if(hostname.includes(",")){
            hostname=hostname.split(",")
        }
        console.log("hostname ",hostname)
        let saveStatus = await gaService.saveBlacklistedData(hostname)
        if(saveStatus.length>0){
            response.push(saveStatus)
        }
    }
    if(reqbody.hasOwnProperty("userid")){
        let userid = reqbody.userid
        if(userid.includes(",")){
            userid=userid.split(",")
        }
        console.log("userid ",userid)
        let saveStatus = await gaService.saveBlacklistedData(userid)
        if(saveStatus.length>0){
            response.push(saveStatus)
        }
    }
    console.log("received response ",response)
    res.send({"blacklist" : response})
}


const pullDataBasedOnUserId= async(req,res)=>{
    let reportdate=req.query.reportdate
    let userid=req.query.userid

    let inputdata={
        startDate : reportdate,
        endDate : reportdate,
        userId : userid
    };
    await gaService.pullDataBasedOnUserId(inputdata)

    res.send({"status" : "in pullDataBasedOnUserId"})
}

const pullDataBasedOnUserIdWire= async(reportdate,userid)=>{

    let inputdata={
        startDate : reportdate,
        endDate : reportdate,
        userId : userid
    };
    await gaService.pullDataBasedOnUserId(inputdata)
}

const pullDataBasedOnUserIdWireFloodData= async()=>{

    let reportDateItems=["2020-02-14"];
    let retUseridArr=[]
    for(let i=0;i<reportDateItems.length;i++){
        //console.log("reportDateItems[i] ",reportDateItems[i])
        retUseridArr = await gaService.getUserIdWithReportDate(reportDateItems[i])
        for(let j=0;j<retUseridArr.length;j++){
            //console.log("retUseridArr[j].userid ",retUseridArr[j].userid)
            await pullDataBasedOnUserIdWire(reportDateItems[i],retUseridArr[j].userid)
        }
    }
    console.log("in pullDataBasedOnUserIdWire end")
}
/*
setTimeout(async()=>{
    await pullDataBasedOnUserIdWireFloodData()
    console.log("settimeout block ended")
},3000)
*/
const deletecsvreport=async(req,res)=>{
    let reportdate=req.query.reportdate
    let returnvalue = await gaService.deletecsvreport(reportdate)
    res.send({status : "in deletecsvreport",...returnvalue})
}
const deleteagencyreport=async(req,res)=>{
    let reportdate=req.query.reportdate
    let returnvalue = await gaService.deleteagencyreport(reportdate)
    res.send({status : "in deleteagencyreport",...returnvalue})
}

const fetchagencydata = async (req,res)=>{

    let fromdate= req.query.fromdate
    let todate= req.query.todate
    try{
        let ret = await gaService.fetchagencydata(fromdate,todate)
        console.log("ret ",ret.length)
        res.send(ret)
    }
    catch(e){
        res.send({status : "error",message:e.message})
    }
}
const fetchagentdata = async (req,res)=>{

    let fromdate= req.query.fromdate
    let todate= req.query.todate
    try{
        let ret = await gaService.fetchagentdata(fromdate,todate)
        console.log("ret ",ret.length)
        res.send(ret)
    }
    catch(e){
        res.send({status : "error",message:e.message})
    }
}

const returnCSVFileInResponse = (data,res,name,opts)=>{
    const csvString = parse(data, opts);
    console.log("csvString ",csvString);

    res.setHeader('Content-disposition', `attachment; filename=${name}-report.csv`);
    res.set('Content-Type', 'text/csv');
    res.status(200).send(csvString);
}

const downloadagencydata=async (req,res)=>{
    console.log("in downloadagencydata")
    const fields = ['city', 'hostname','countryIsoCode','noOfusers'
    ,'pageviews','timeOnPage','reportDate'];
    const opts = { fields };

    try{
        let fromdate = "2020-01-20"
        let todate="2120-02-16"
        let ret = await gaService.fetchagencydata(fromdate,todate,"noid")
        console.log("ret ",ret)
        returnCSVFileInResponse(ret,res,"agency",opts)
    }
    catch(e){
        res.send({status : "error",message:e.message})
    }
    

}
/*
setTimeout(()=>{
    //downloadagencydata()
})
*/
const downloadagentdata= async(req,res)=>{
    console.log("in downloadagentdata")
    const fields = ['userid', 'hostname','averageSessionDuration','activity'
    ,'reportdate'];
    const opts = { fields };
    try{
        let fromdate = "2020-01-20"
        let todate="2120-02-16"
        let ret = await gaService.fetchagentdata(fromdate,todate,"noid")


        ret = ret.map((data,index)=>{
            if(data.activity){
                return {
                    userid : data.userid,
                    hostname: data.hostname,
                    averageSessionDuration : data.averageSessionDuration,
                    activity:data.activity.length,
                    reportdate: data.reportdate
                }
            }
            return data
        })
        console.log("ret ",ret.length)
        returnCSVFileInResponse(ret,res,"agent",opts)
    }
    catch(e){
        res.send({status : "error",message:e.message})
    }
}
const downloadagentdatawithactivity= async(req,res)=>{
    console.log("in downloadagentdatawithactivity")
    const fields = ['userid', 'hostname','averageSessionDuration','activityData'
    ,'reportdate'];
    const opts = { fields };
    try{
        let fromdate = "2020-01-20"
        let todate="2120-02-16"
        let ret = await gaService.fetchagentdata(fromdate,todate,"noid")


        ret = ret.map((data,index)=>{
            if(data.activity){
                let withactivity= data.activity.map(activityData=>{
                    return {
                        userid : data.userid,
                        hostname: data.hostname,
                        averageSessionDuration : data.averageSessionDuration,
                        activityData: activityData.pagePath,
                        reportdate: data.reportdate
                    }
                })
                return withactivity
            }
            return data
        })
        let properdata=[]
        for(let i=0;i<ret.length;i++){
            properdata =[...properdata,...ret[i]]
        }
        console.log("ret.length ",ret.length)
        console.log("properdata ",properdata)

        returnCSVFileInResponse(properdata,res,"agent-with-activitydata",opts)
    }
    catch(e){
        res.send({status : "error",message:e.message})
    }
}
module.exports={
    loadcsvData,loadAgencyData,postBlacklist,pullDataBasedOnUserId,
    deletecsvreport,deleteagencyreport,fetchagencydata,fetchagentdata,downloadagencydata,downloadagentdata,
    downloadagentdatawithactivity
}