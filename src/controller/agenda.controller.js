const agenda = require("./../config/agenda.config");

const immediate = async (req,res)=>{
    console.log("in agenda.controller.js scheduleimmediate")
    let data = {
        name:"divine",
        city:"blr"
    }
    await agenda.now('scheduleimmediate',data);
    res.send({status : "from scheduleimmediate"})
}
const scheduleafter = async (req,res)=>{
    console.log("agent.controller.js scheduleLate start")
    let data = {
        name:req.query.name,
        city:req.query.city
    }
    const job = agenda.create("scheduleafter",data);
    job.schedule('2 minutes');

    await job.save()
    console.log("agent.controller.js scheduleLate end")
    res.send({status : "in scheduleLate ",})
}

const scheduleafterheavy = async (req,res)=>{
    console.log("agent.controller.js scheduleafterheavy start")
    let data = {
        name:req.query.name,
        city:req.query.city
    }
    const job = agenda.create("scheduleafterheavy",data);
    job.schedule('1 minutes');

    await job.save()
    console.log("agent.controller.js scheduleafterheavy end")
    res.send({status : "in scheduleafterheavy ",})
}

const stopagenda = async (req,res)=>{
    console.log("in stopagenda")
    await agenda.stop();
    res.send({status:"in stopagenda"})
}

const startagenda = async (req,res)=>{
    console.log("in startagenda")
    await agenda.start();
    res.send({status:"in startagenda"})
}

/*
0 6 * * *
seconds, minutes, hours, days,weeks, months -- assumes 30 days, years -- assumes 365 days

0 6 0 0 0 0 0
*/
/*
setTimeout(async ()=>{
    console.log("33---")
    await scheduleLate()
    console.log("35---")
},3000)
*/
const purge = async (req,res)=>{
    console.log("in agenda.controller.js purge")
    await agenda.purge();
    res.send({status : "from purge"})
}
module.exports={
    purge,immediate,scheduleafter,scheduleafterheavy,stopagenda,startagenda
}