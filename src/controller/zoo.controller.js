const zooModel = require("./../models/zoo.model")


const dataget = async (req,res)=>{  
    console.log("zoo.controller.js dataget")
    let re = await zooModel.find({})
    re.forEach((data,ind)=>{
        console.log("data name %s location %s ",data.name,data.location)
    })
    console.log("re ",re)
    res.send({status : "from dataget",re})
}



const datapost  = async (req,res)=>{  
    console.log("zoo.controller.js datapost")
    let zz = new zooModel(req.body)
    let re = await zz.save()

    console.log("re ",re)
    res.send({status : "from datapost",re})
}

const dataupdate = async (req,res)=>{  
    console.log("zoo.controller.js datapost")
    const id = req.query.id
    console.log("id ",id)
    let re = await zooModel.findByIdAndUpdate(id,req.body)
    console.log("re ",res)
    res.send({status : "from dataupdate",re})
}

module.exports={dataget,datapost,dataupdate}