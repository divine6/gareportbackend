var Queue = require('bull');
const { setQueues } = require('bull-board')

var emailQueue = new Queue('email workflow', 'redis://127.0.0.1:6379');
setQueues([emailQueue])
console.log("bull.config start")
emailQueue.process(function(job, done){
    // job.data contains the custom data passed when the job was created
    // job.id contains id of this job.

    console.log("job.data ",job.data)
    console.log("job.id ",job.id)
    new Promise((resolve,reject)=>{
        setTimeout(()=>{
           
            resolve()
        },20000)
    }).then(()=>{
        done(null,{status:"queue data"});
        console.log("job ended")
    })
});

//emailQueue.add({video: 'http://example.com/video1.mov'});


emailQueue.pause().then(function(){
    // queue is paused now
    console.log("emailQueue paused ")
});

emailQueue.resume().then(function(){
    // queue is resumed now
    console.log("emailQueue resume ")
})

emailQueue.on('completed', function(job, result){
    // Job completed with output result!
    console.log("emailQueue completed ")
    console.log("job.id " ,job.id, " -result- ",result)
})

emailQueue.on('removed', function(job){
    // A job successfully removed.
    console.log("emailQueue removed ")
    console.log("job.id " ,job.id)
});


emailQueue.on('waiting', function(jobId){
    // A Job is waiting to be processed as soon as a worker is idling.
    console.log("emailQueue waiting ")

});
  
emailQueue.on('active', function(job, jobPromise){
    // A job has started. You can use `jobPromise.cancel()`` to abort it.
    console.log("emailQueue active ")
})

emailQueue.on('progress', function(job, progress){
    // A job's progress was updated!
    console.log("emailQueue progress ")
})
  

emailQueue.on('stalled', function(job){
    // A job has been marked as stalled. This is useful for debugging job
    // workers that crash or pause the event loop.
    console.log("emailQueue stalled ")
})

console.log("bull.config end")

module.exports=emailQueue