'use strict';
/*eslint no-process-env:0*/

// Development specific configuration
// ==================================
module.exports = {

  // MongoDB connection options
  mongo: {
    //  uri : 'mongodb://localhost/gareports',
      uri: 'mongodb://testinsuredmine:UFtGzMZ6e8tHH6BHgKKPyrtg4vTtHF9ku5KWcS2TrRySgsqRaEaYsgbY5VRuYZPq@35.160.216.11:27017/testinsuredmine',
      options : {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        poolSize: 20,
        connectTimeoutMS:360000,
        serverSelectionTimeoutMS: 50000,
        useCreateIndex : true,
        useFindAndModify : false,
    }
  }
};

