const Agenda = require("agenda")

const dbconfig = require("./dbconfig")

let agenda;

(async()=>{
    console.log("in agenda start")

    agenda = new Agenda({
        db: {
            address: dbconfig.mongo.uri, collection: 'agendajobs'
        }
    });
    agenda.on('ready', async () =>{
        console.log("in agenda ready start")

        agenda.define('scheduleimmediate',  (job,done) => { 
            console.log('scheduleimmediate - start');
            console.log("job ",job)
            console.log("job.attrs ",job.attrs)
            console.log("jobx.data ",job.attrs.data)
            done()
            console.log('scheduleimmediate - end');
        });
        
        agenda.define('scheduleafter',  (job,done) => { 
            console.log('scheduleafter job started');
            console.log("job ",job)
            console.log("job.attrs ",job.attrs)
            console.log("job.data ",job.attrs.data)
            done()
            console.log('scheduleafter job ended');
        });
        agenda.define('scheduleafterheavy',
        {
            concurrency : 1
        }
        ,  (job,done) => { 
            console.log('scheduleafterheavy job started');
            console.log("job ",job)
            console.log("job.attrs ",job.attrs)


            let p = new Promise((resolve,reject)=>{
                console.log("inside promise")
                setTimeout(()=>{
                    console.log("promise completed")
                    resolve()
                },15000)
            })
            p.then(()=>{
                console.log("promise then invoked")
                done()
            })
            console.log("job.data ",job.attrs.data)
           
            console.log('scheduleafterheavy job ended');
        });

       
        await agenda.start();
        console.log("in agenda ready end")
    })

    agenda.on('start', job => {
        console.log('Job %s starting', job.attrs.name);
    });
    agenda.on('complete', job => {
        console.log(`Job ${job.attrs.name} finished`);
    });
    console.log("in agenda end")

})()

module.exports=agenda