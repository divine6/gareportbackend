const axios = require("axios")
const ga_config =require("./../config/ga.apiconfig")
const ga_blacklisted_model = require("./../models/ga_blacklist.model")
const ga_userid_model = require("./../models/ga_userid.model");
const csv = require('ya-csv');
const useridPullData = require("./../../seed/useriddata.pull.json")
//
const ga_agency_data_model = require("./../models/ga_agency_data.model")
const ga_trackreport_model = require("./../models/ga_trackreport.model")


const getBlackLists =  ()=>{
    return ga_blacklisted_model.find({},'hostname_or_userid')
}

//invoke GA API to fetch data
const getPromiseForLoadCsv=(filename,blacklists,reportdate)=>{
    return new Promise((resolve,reject)=>{
        var reader = csv.createCsvFileReader(filename, {
            'separator': ',',
            'quote': '"',
            'escape': '"',       
            'comment': '',
        });
        let useridDump =[]
        reader.addListener('data', function(data) {
            let userid = data[0]
            let sessions = data[1]
            let averageSessionDuration = data[2]
            console.log("userid ",userid)
            let flag = blacklists.find((list)=>{
                console.log("obj ",list["hostname_or_userid"])
                if(list["hostname_or_userid"] == userid){
                    console.log("userid exists")
                    return true;
                }
                else{
                    console.log("userid not exists")
                    return false;
                }
            })
            if(flag){
            }else{
                useridDump.push({userid,sessions,averageSessionDuration,reportdate})
            }
        });
        reader.addListener('error',function(data){
            console.log("error ",data)
            reject("error during parsing csv")
        })
        reader.addListener('end',function(){
            resolve(useridDump)
        })  
    })
}

const loadCsv = async (reportdate,filename)=>{
    try{
        let blacklists = await getBlackLists();
        console.log("blacklists ",blacklists)
        let userIdDump = await getPromiseForLoadCsv(filename,blacklists,reportdate)
        console.log("userIdDump ",userIdDump)
        await ga_userid_model.insertMany(userIdDump)
    }
    catch(e){
        console.log("ga.service.js loadCsv-exception ",e)
    }
}
const etlAgenciesFromExternalSource = async (reportdate)=>{
    let start_date = reportdate
    let end_date=reportdate
    
    let access_token = ga_config.access_token_value
    let apiurl = ga_config.ga_agency_data_url;
    let nextLink = null
    
    console.log("before apiurl ",apiurl)
    apiurl = apiurl.replace(ga_config.start_date,start_date)
    apiurl = apiurl.replace(ga_config.end_date,end_date)
    apiurl = apiurl.replace(ga_config.access_token,access_token)
    let resultdata =  null
    try{
        do{
            console.log("entering dowhile")
            nextLink = null

            console.log("ajax apiurl ",apiurl)

            resultdata = await axios.get(apiurl)    
            await agencyDetails(resultdata.data)    
            nextLink = resultdata.data.hasOwnProperty("nextLink") ? resultdata.data.nextLink : null
            console.log("nextLink ",nextLink)
            apiurl=`${nextLink}&access_token=${access_token}`
        }while(nextLink!=null)
    }
    catch(e){
        throw e;
    }
}

const saveBlacklistedData =async (value)=>{
    console.log("saveBlacklistedData.service - value ",value)
    let response = []
    for(let i=0;i<value.length;i++){
        let inputobj = {"hostname_or_userid" : value[i]}
        console.log("inputobj ",inputobj)
        let obj = new ga_blacklisted_model(inputobj)
        try{
            await obj.save()
        }
        catch(e){
            console.log("e ",e.errmsg)
            response.push({errmsg : e.errmsg,"hostname_or_userid": value[i]})
        }
    }
    return response
}


const agencyDetails = async (ga_agency_data)=>{
    console.log("122-ga_agency_data ",ga_agency_data)
    let rows = ga_agency_data.rows
    //2020-02-05
    let reportDate = ga_agency_data.query["start-date"]
    console.log("agencyDetails reportDate- ",reportDate)

    let blacklists = await getBlackLists();
    console.log("blacklists ",blacklists)

    let gaAgencyData = rows.filter(row=>{
    let hostname = row[3]
    let flag = blacklists.find((list)=>{
        return list["hostname_or_userid"] == hostname
    })
    return flag ? false : true
    }).map(row=>{
        let city = row[0]
        let latitude = row[1]
        let longitude = row[2]
        let hostname = row[3]
        let pagePath = row[4]
        let cityId = row[5]
        let countryIsoCode = row[6]
        let noOfusers= row[7]
        let pageviews = row[8]
        let timeOnPage = row[9]
        return {city,latitude,longitude,hostname,pagePath,cityId,countryIsoCode,noOfusers,pageviews,
            timeOnPage,reportDate}
    })
    
    try{
        let insertmany = await ga_agency_data_model.insertMany(gaAgencyData)
        console.log("insertmany result ")
    }
    catch(e){
        console.log("insertMany e ",e)
    }
}

const isReportDateExist = (inputdata)=>{
    return ga_trackreport_model.findOne(inputdata)
} 

const insertReportMetadata=(finddata,inputdata)=>{
    return ga_trackreport_model.findOneAndUpdate(finddata,{...inputdata,updatedAt:Date.now()},{
        new : true,upsert : true,runValidators:true
    })
}

const pullDataBasedOnUserId = async (inputdata)=>{
    console.log("in pullDataBasedOnUserId")

    let requrl = ga_config.ga_userid_pull_url
    let reqbody = ga_config.ga_userid_pull_body
    let access_token = ga_config.userid_pull_accesstoken

    reqbody.dateRange.startDate = inputdata.startDate
    reqbody.dateRange.endDate = inputdata.endDate
    reqbody.user.userId = inputdata.userId

    let headers = {
        headers: {
            Authorization: 'Bearer ' + access_token
        }
    }
    console.log("requrl ",requrl)
    console.log("headers ",headers)
    console.log("reqbody ",reqbody)

    let userResponse = null
    try{
        userResponse = await axios.post(requrl,reqbody,headers)
        await transformLoadUserIdData(inputdata.userId,userResponse.data,inputdata.startDate)
        console.log("userResponse.data " ,JSON.stringify(userResponse.data))
    }
    catch(e){
        console.log("userid catchblock ",e)
    }
}
const transformLoadUserIdData = async (userid,useridData,reportdate)=>{
    console.log("in transformLoadUserIdData ")
    let useractivity={
        activity:[],
        hostname :""
    }
    let hostname=null
    useractivity.userid=userid
    useridData.sessions.forEach(session => {
        let sessionId =session.sessionId
        session.activities.forEach(activity=>{
            if(hostname==null){
                hostname=activity.hostname ? activity.hostname : null
            }
            let activityTime = activity.activityTime
            let activityType = activity.activityType
            let landingPagePath = activity.landingPagePath
            let pagePath = activity.pageview.pagePath
            let pageTitle = activity.pageview.pageTitle
            useractivity.activity.push({sessionId,activityTime,activityType,landingPagePath,pagePath,pageTitle})
        })
    });
    useractivity.hostname=hostname
    try{
        let ret = await ga_userid_model.findOneAndUpdate(
            {userid:userid,reportdate: reportdate},{...useractivity},{new:true})
        console.log("retvalue ",ret)
    }
    catch(e){
        console.log("exception-e- ",e)
    }
    console.log("useractivity end")
}
// setTimeout(async ()=>{
//     await transformLoadUserIdData("ben1@insuredmine.com",useridPullData,"2020-02-05")
// },4000)
const deletecsvreport = async(reportdate)=>{
    let re1 =await ga_userid_model.deleteMany({reportdate})
    console.log("re1 ",re1)
    let re2 =await ga_trackreport_model.deleteOne({reportdate})
    console.log("re2 ",re2)
    return{
        ga_userid_model:{...re1},
        ga_trackreport_model:{...re2}
    }
}
const deleteagencyreport = async(reportdate)=>{
    
    let re1 =await ga_agency_data_model.deleteMany({reportdate})
    console.log("re1 ",re1)
    let re2 =await ga_trackreport_model.deleteOne({reportdate})
    console.log("re2 ",re2)
    return{
        ga_agency_data_model:{...re1},
        ga_trackreport_model:{...re2}
    }
}
const fetchagencydata= async(fromdate,todate,condition)=>{
    if(condition){
        return ga_agency_data_model.find({}).where("reportDate")
        .gte(fromdate).lte(todate)
        .select('-_id city hostname countryIsoCode noOfusers pageviews timeOnPage reportDate')

    }

    return ga_agency_data_model.find({}).where("reportDate").gte(fromdate).lte(todate).select('city hostname countryIsoCode noOfusers pageviews timeOnPage reportDate')
}

const fetchagentdata= async(fromdate,todate,condition)=>{
    if(condition){
        return ga_userid_model.find({}).where("reportdate").gte(fromdate).lte(todate)
        .select('-_id userid hostname averageSessionDuration activity reportdate')    
    }
    return ga_userid_model.find({}).where("reportdate").gte(fromdate).lte(todate).select('userid hostname averageSessionDuration activity reportdate')
}

const getUserIdWithReportDate= (reportdate)=>{
    return ga_userid_model.find({reportdate:reportdate},'userid')

}
/*
const dummy= ()=>{
    return ga_userid_model.find({userid : "tate@ammteam.net",reportdate :"2020-01-20"},
    'userid hostname averageSessionDuration activity reportdate')
}
setTimeout(async ()=>{
   let ret =  await dummy()
   console.log("ret ",ret)
},3000)
*/
module.exports={
    saveBlacklistedData,isReportDateExist,insertReportMetadata,etlAgenciesFromExternalSource,
    loadCsv,pullDataBasedOnUserId,deletecsvreport,deleteagencyreport,fetchagencydata,fetchagentdata,getUserIdWithReportDate
}